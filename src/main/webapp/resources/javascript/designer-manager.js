//---IO
function showAsXml() {
	var xmlString = designerWidget.toXmlString();
	document.getElementById("textarea").value = xmlString;
}

function loadFromXml() {
	var xml = document.getElementById("textarea").value;
	designerWidget.fromXmlString(xml);
}

function initialDefinitions(){
	
//	document.getElementById("contextMenuForm:cMenuAddTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
//	document.getElementById("contextMenuForm:cMenuAddTable").setAttribute("onclick", "");
//	document.getElementById("contextMenuForm:cMenuAlignTables").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
//	document.getElementById("contextMenuForm:cMenuAlignTables").setAttribute("onclick", "");
//	document.getElementById("contextMenuForm:cMenuClearTables").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
//	document.getElementById("contextMenuForm:cMenuClearTables").setAttribute("onclick", "");
	
	
	var elements = document.getElementById("contextMenuForm:contextMenuId").getElementsByTagName("a");
	for(var a=0;a<elements.length;a++){
		var idelement = elements[a].id;
		if(idelement){
			document.getElementById(idelement).setAttribute("onmouseover", "setItemMenuOver(this.id)");
			document.getElementById(idelement).setAttribute("onmouseout", "setItemMenuOut(this.id)");
		}
	}
	
	
	
	onUniqueTableDeselection();
	onUniqueRowDeselection();
	
//	document.getElementById("contextMenuForm:cMenuAddTable").setAttribute("class", "ui-menuitem-link ui-corner-all");
//	document.getElementById("contextMenuForm:cMenuAddTable").setAttribute("onclick", "designerWidget.tableManager.preAdd()");
//	document.getElementById("contextMenuForm:cMenuAlignTables").setAttribute("class", "ui-menuitem-link ui-corner-all");
//	document.getElementById("contextMenuForm:cMenuAlignTables").setAttribute("onclick", "designerWidget.alignTables()");		
//	document.getElementById("contextMenuForm:cMenuClearTables").setAttribute("class", "ui-menuitem-link ui-corner-all");
//	document.getElementById("contextMenuForm:cMenuClearTables").setAttribute("onclick", "designerWidget.tableManager.clear()");
	
	

}

function onUniqueTableSelection(){
	
	//toolbar menu
	document.getElementById("menuAddRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
	document.getElementById("menuAddRow").setAttribute("onclick", "designerWidget.tableManager.addRow()");
	
	document.getElementById("menuEditTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
	document.getElementById("menuEditTable").setAttribute("onclick", "designerWidget.tableManager.edit()");
	
	document.getElementById("menuTableKeys").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
	document.getElementById("menuTableKeys").setAttribute("onclick", "designerWidget.tableManager.keys()");
	
	if(designerWidget.tableManager.selection.length){
		document.getElementById("menuRemoveTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
		document.getElementById("menuRemoveTable").setAttribute("onclick", "designerWidget.tableManager.remove()");
	}

	//context menu																	
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onclick", "designerWidget.tableManager.addRow()");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onmouseover", "setItemMenuOver(this.id)");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onclick", "designerWidget.tableManager.edit();");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onmouseover", "setItemMenuOver(this.id)");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onclick", "designerWidget.tableManager.keys()");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onmouseover", "setItemMenuOver(this.id)");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	
	if(designerWidget.tableManager.selection.length){
		document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("class", "ui-menuitem-link ui-corner-all");
		document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onclick", "designerWidget.tableManager.remove()");
		document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onmouseover", "setItemMenuOver(this.id)");
		document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	}
}

function onUniqueTableDeselection(){
	
	//toolbar menu
	document.getElementById("menuAddRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuAddRow").setAttribute("onclick", "");
	
	document.getElementById("menuEditTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuEditTable").setAttribute("onclick", "");
	
	document.getElementById("menuTableKeys").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuTableKeys").setAttribute("onclick", "");
	
	document.getElementById("menuRemoveTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuRemoveTable").setAttribute("onclick", "");

	//context menu												
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuAddRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuEditTable").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuTableKeys").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
	document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuRemoveTable").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
}

function onUniqueRowSelection(){
	
	var table = designerWidget.rowManager.selected.owner;
	
	if(!table)
		return;
	
	var rows = table.rows;

	//toolbar menu
	document.getElementById("menuEditRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
	document.getElementById("menuEditRow").setAttribute("onclick", "designerWidget.rowManager.edit()");
	
	document.getElementById("menuRemoveRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
	document.getElementById("menuRemoveRow").setAttribute("onclick", "designerWidget.rowManager.remove()");
	
	//context menu 
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onclick", "designerWidget.rowManager.edit()");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onmouseover", "setItemMenuOver(this.id)");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onclick", "designerWidget.rowManager.remove()");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onmouseover", "setItemMenuOver(this.id)");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	
	if( ! (rows[0] == designerWidget.rowManager.selected) ){
		
		//toolbar menu
		document.getElementById("menuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
		document.getElementById("menuUpRow").setAttribute("onclick", "designerWidget.rowManager.up()");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onclick", "designerWidget.rowManager.up()");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseover", "setItemMenuOver(this.id)");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseout", "setItemMenuOut(this.id)");
		
	}else{
		
		//toolbar menu
		document.getElementById("menuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
		document.getElementById("menuUpRow").setAttribute("onclick", "");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onclick", "");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
		document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	}
	
	if( ! (rows[rows.length-1] == designerWidget.rowManager.selected) ){
		
		//toolbar menu
		document.getElementById("menuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
		document.getElementById("menuDownRow").setAttribute("onclick", "designerWidget.rowManager.down()");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onclick", "designerWidget.rowManager.down()");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseover", "setItemMenuOver(this.id)");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseout", "setItemMenuOut(this.id)");
	}else{
		
		//toolbar menu
		document.getElementById("menuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
		document.getElementById("menuDownRow").setAttribute("onclick", "");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onclick", "");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
		document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	}
	
	if( designerWidget.rowManager.selected.isUnique() ){
		
		//toolbar menu
		document.getElementById("menuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
		document.getElementById("menuCreateFK").setAttribute("onclick", "designerWidget.rowManager.foreigncreate()");
		
		document.getElementById("menuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
		document.getElementById("menuConnectFK").setAttribute("onclick", "designerWidget.rowManager.foreignconnect()");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onclick", "designerWidget.rowManager.foreigncreate()");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseover", "setItemMenuOver(this.id)");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseout", "setItemMenuOut(this.id)");
		
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onclick", "designerWidget.rowManager.foreignconnect()");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseover", "setItemMenuOver(this.id)");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseout", "setItemMenuOut(this.id)");
		
	}else{
		
		//toolbar menu
		document.getElementById("menuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
		document.getElementById("menuCreateFK").setAttribute("onclick", "");
		
		document.getElementById("menuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
		document.getElementById("menuConnectFK").setAttribute("onclick", "");
		
		//context menu 
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onclick", "");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
		document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
		
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onclick", "");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
		document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");
	
		
	}
	
	//toolbar menu
	document.getElementById("menuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuDisconnectFK").setAttribute("onclick", "designerWidget.rowManager.foreigndisconnect()");
	
	//context menu 
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onclick", "designerWidget.rowManager.foreigndisconnect()");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	var rels = designerWidget.rowManager.selected.relations;
	for (var i=0;i<rels.length;i++) {
		var r = rels[i];
		if (r.row2 == designerWidget.rowManager.selected) { 
			
			//toolbar menu
			document.getElementById("menuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input");
			document.getElementById("menuDisconnectFK").setAttribute("onclick", "designerWidget.rowManager.foreigndisconnect()");
			
			//context menu 
			document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all");
			document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onclick", "designerWidget.rowManager.foreigndisconnect()");
			document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseover", "setItemMenuOver(this.id)");
			document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseout", "setItemMenuOut(this.id)");
			
		}
	}
}

function onUniqueRowDeselection(){
	
	//toolbar menu
	document.getElementById("menuEditRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuEditRow").setAttribute("onclick", "");
	
	document.getElementById("menuRemoveRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuRemoveRow").setAttribute("onclick", "");
	
	document.getElementById("menuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuUpRow").setAttribute("onclick", "");
	
	document.getElementById("menuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuDownRow").setAttribute("onclick", "");
	
	document.getElementById("menuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuCreateFK").setAttribute("onclick", "");
	
	document.getElementById("menuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuConnectFK").setAttribute("onclick", "");
	
	document.getElementById("menuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-bar-input ui-state-disabled");
	document.getElementById("menuDisconnectFK").setAttribute("onclick", "");
	
	//context menu 
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuEditRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuRemoveRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuUpRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuDownRow").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuCreateFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuConnectFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onclick", "");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseover", "setItemMenuDesactived(this.id)");
	document.getElementById("contextMenuForm:cMenuDisconnectFK").setAttribute("onmouseout", "setItemMenuDesactived(this.id)");

}

//function setAlteredOptions(snapDistance, foreignNamesPattern, hideConnectors, drawSmoothConnectors, showFieldSize, showDatatype){
//	
//	alert("snap = "+snapDistance);
//	alert("pattern = "+foreignNamesPattern);
//	alert("hide = "+hideConnectors);
//	alert("vector = "+drawSmoothConnectors);
//	alert("showsize = "+showFieldSize);
//	alert("showtype = "+showDatatype);
//	
//	
//	designerWidget.setOption("snap", snapDistance);
//	designerWidget.setOption("pattern", foreignNamesPattern);
//	designerWidget.setOption("hide", hideConnectors);
//	designerWidget.setOption("vector", drawSmoothConnectors);
//	designerWidget.setOption("showsize", showFieldSize);
//	designerWidget.setOption("showtype", showDatatype);
//}


function setItemMenuOver(id){
	document.getElementById(id).setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-hover");
	document.getElementById(id).parentNode.setAttribute("class", "ui-menuitem ui-widget ui-corner-all ui-menuitem-active");
}

function setItemMenuOut(id){
	document.getElementById(id).setAttribute("class", "ui-menuitem-link ui-corner-all");
	document.getElementById(id).parentNode.setAttribute("class", "ui-menuitem ui-widget ui-corner-all");
}

function setItemMenuDesactived(id){
	document.getElementById(id).setAttribute("class", "ui-menuitem-link ui-corner-all ui-state-disabled");
	document.getElementById(id).parentNode.setAttribute("class", "ui-menuitem ui-widget ui-corner-all");
}

