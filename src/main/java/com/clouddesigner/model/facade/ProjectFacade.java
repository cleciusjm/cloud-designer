package com.clouddesigner.model.facade;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.clouddesigner.component.model.DbModel;
import com.clouddesigner.component.model.Key;
import com.clouddesigner.component.model.ModelTable;
import com.clouddesigner.component.model.Relation;
import com.clouddesigner.component.model.TableRow;
import com.clouddesigner.entities.AllowedUser;
import com.clouddesigner.entities.Project;
import com.clouddesigner.entities.User;
import com.clouddesigner.model.facade.interceptors.BenchmarkMethodInterceptor;

@LocalBean
@Stateless
@Interceptors(value = { BenchmarkMethodInterceptor.class })
public class ProjectFacade {
	@PersistenceContext
	private EntityManager entityManager;

	@Lock(LockType.READ)
	public List<Project> getAllProjectList(User user) {
		return entityManager
				.createNamedQuery("Project.getByOwnerByAllowedUser",
						Project.class).setParameter("user", user)
				.getResultList();
	}

	@Lock(LockType.READ)
	public List<Project> getOwnedProjects(User user) {
		return entityManager
				.createNamedQuery("Project.getByUserAllowed", Project.class)
				.setParameter("user", user).getResultList();
	}

	public Project insert(Project project) {
		project = entityManager.merge(project);
		if (project.getModel().getTables() != null) {
			for (ModelTable mt : project.getModel().getTables()) {
				mt.setModel(project.getModel());
				if (mt.getKeys() != null) {
					for (Key k : mt.getKeys()) {
						k.setTable(mt);
					}
				}
				if (mt.getRows() != null) {
					for (TableRow tr : mt.getRows()) {

						tr.setTable(mt);
						if (tr.getRelations() != null) {
							for (Relation r : tr.getRelations()) {
								r.setOwnerRow(tr);
							}
						}

					}
				}
			}
		}
		return entityManager.merge(project);
	}

	public DbModel mergeModel(DbModel model) {
		model = entityManager.merge(model);
		if (model.getTables() != null) {
			for (ModelTable mt : model.getTables()) {
				mt.setModel(model);
				if (mt.getKeys() != null) {
					for (Key k : mt.getKeys()) {
						k.setTable(mt);
					}
				}
				if (mt.getRows() != null) {
					for (TableRow tr : mt.getRows()) {

						tr.setTable(mt);
						if (tr.getRelations() != null) {
							for (Relation r : tr.getRelations()) {
								r.setOwnerRow(tr);
							}
						}

					}
				}
			}
		}
		return entityManager.merge(model);
	}

	public Project update(Project project) {
		return entityManager.merge(project);
	}

	public void remove(Project project) {
		entityManager
				.remove(entityManager.find(Project.class, project.getId()));
	}

	public void removeAllowedUser(AllowedUser allowedUser) {
		allowedUser = entityManager
				.find(AllowedUser.class, allowedUser.getId());
		entityManager.remove(allowedUser);
	}

	public Project merge(Project project) {
		return entityManager.merge(project);
	}

	public Project find(Integer id) {
		return entityManager.find(Project.class, id);
	}

	public Project getProjectByIdByOwner(int id, User user) {
		return entityManager
				.createNamedQuery("Project.getByIdByOwner", Project.class)
				.setParameter("id", id).setParameter("user", user)
				.getSingleResult();
	}

	public Project getProjectByIdByUser(int id, User user) {
		return entityManager
				.createNamedQuery("Project.getByIdByOwnerByAllowedUser",
						Project.class).setParameter("user", user)
				.setParameter("id", id).getSingleResult();
	}

	public void removeModelById(Integer idModel) {
		entityManager.remove(entityManager.find(DbModel.class, idModel));
	}
}
