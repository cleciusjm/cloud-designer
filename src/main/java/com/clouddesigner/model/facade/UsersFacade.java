package com.clouddesigner.model.facade;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.clouddesigner.entities.User;
import com.clouddesigner.model.facade.interceptors.BenchmarkMethodInterceptor;

@LocalBean
@Stateless
@Interceptors(value = { BenchmarkMethodInterceptor.class })
public class UsersFacade {

	@PersistenceContext
	private EntityManager entityManager;

	@Lock(LockType.READ)
	public List<User> getAllUsers() {
		return entityManager.createNamedQuery("User.getAll", User.class).getResultList();
	}

	public User getByLogin(String login) {
		TypedQuery<User> createNamedQuery = entityManager.createNamedQuery("User.getByLogin", User.class);
		createNamedQuery.setParameter("login", login);
		return createNamedQuery.getSingleResult();
	}
	
	public User getById(Integer id) {
		TypedQuery<User> createNamedQuery = entityManager.createNamedQuery("User.getById", User.class);
		createNamedQuery.setParameter("id", id);
		return createNamedQuery.getSingleResult();
	}

	public User insert(User user) {
		return entityManager.merge(user);
	}

	public User update(User user) {
		return entityManager.merge(user);
	}

	public void remove(User user) {
		User record = entityManager.find(User.class, user.getId());
		entityManager.remove(record);
	}
}
