package com.clouddesigner.model.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.clouddesigner.entities.AllowedUser;
import com.clouddesigner.entities.Project;
import com.clouddesigner.entities.User;
import com.clouddesigner.entities.transients.Permission;
import com.clouddesigner.model.facade.interceptors.BenchmarkMethodInterceptor;
import com.clouddesigner.util.log.Log;

@Singleton
@LocalBean
@Interceptors(value = { BenchmarkMethodInterceptor.class })
public class ProjectInstancesManagerFacade {

	private Map<Integer, Set<Permission>> permissionsList = new HashMap<>();

	@Inject
	private Log log;
	
	@Inject
	UsersFacade usersFacade;

	public void closeProject(Project project, User user) {
		remove(project.getId(), user.getId());
	}

	public Permission openProject(Project project, User user) {
		try {
			Permission permission = null;
			if (project.getOwner().getId().equals(user.getId())) {
				permission = new Permission(user.getId(), Permission.OWNER);
			} else {
				for (AllowedUser allowedUser : project.getAllowedUsers()) {
					if (allowedUser.getUser().getId().equals(user.getId())) {
						permission = new Permission(user.getId(), allowedUser.isReadOnly() ? Permission.READ : Permission.EDIT);
						break;
					}
				}

			}
			if (permission != null) {
//				put(project.getId(), permission);
			} else {
				permission = new Permission(user.getId(), Permission.NONE);
			}
			return permission;
		} catch (Exception e) {
			log.e("ERRO no openProject {0}", e, e.getMessage());
			throw e;
		}
	}

	private void put(final int projectId, final Permission user) {
		if (permissionsList.get(projectId) == null) {
			permissionsList.put(projectId, new HashSet<Permission>());
		}
		permissionsList.get(projectId).add(user);
	}

	private void remove(final int projectId, final int userId) {
		Set<Permission> list = permissionsList.get(projectId);
		if (list != null) {
			Permission toRemove = null;
			for (Permission permission : list) {
				if (permission.getUserId() == userId) {
					toRemove = permission;
					break;
				}
			}
			list.remove(toRemove);
			System.out.println("removeu: "+list.size());
			if(list.size()==0)
				permissionsList.remove(projectId);
		}
	}
	

	
	public List<User> getUsersInEdition(int projectId){
		List<User> users = new ArrayList<>();
		Set<Permission> permissionList = permissionsList.get(projectId);
		if (permissionList != null) {
			for (Permission permission : permissionList) {
				users.add(usersFacade.getById(permission.getUserId()));
			}
		}
		return users;
	}

}
