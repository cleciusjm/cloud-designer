package com.clouddesigner.model.facade;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.clouddesigner.entities.Role;
import com.clouddesigner.model.facade.interceptors.BenchmarkMethodInterceptor;

@LocalBean
@Stateless
@Interceptors(value = { BenchmarkMethodInterceptor.class })
public class RolesFacade {

	@PersistenceContext
	private EntityManager entityManager;

	public List<Role> getAllRoles() {
		return entityManager.createNamedQuery("Role.getAll", Role.class).getResultList();
	}

}
