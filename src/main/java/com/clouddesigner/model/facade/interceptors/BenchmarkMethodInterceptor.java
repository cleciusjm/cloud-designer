package com.clouddesigner.model.facade.interceptors;

import java.io.Serializable;
import java.lang.reflect.Method;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.clouddesigner.model.facade.InternalParameter;
import com.clouddesigner.model.facade.ParamsManagerFacade;
import com.clouddesigner.util.log.Log;

public class BenchmarkMethodInterceptor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private Log log;
	@EJB
	private ParamsManagerFacade paramsManagerFacade;

	@AroundInvoke
	public Object benchmarkMethodCall(InvocationContext context) throws Exception {
		Object resp = null;
		if ((Boolean) paramsManagerFacade.getParamsMap().get(InternalParameter.LOG_BENCHMARK)) {
			StringBuilder logMessage = new StringBuilder();
			logMessage.append(logMethod(context.getTarget().getClass(), context.getMethod(), context.getParameters()));
			long preExec = System.currentTimeMillis();
			resp = context.proceed();
			long postExec = System.currentTimeMillis();
			logMessage.append(" : ExecTime[").append(postExec - preExec).append(" milisegundos]");
			log.d(logMessage.toString());
		} else {
			resp = context.proceed();
		}
		return resp;
	}

	private String logMethod(Class<? extends Object> classe, Method metodo, Object... parametros) {
		StringBuilder methodLog = new StringBuilder();
		methodLog.append(classe.getName()).append(".").append(metodo.getName());
		methodLog.append("(");
		for (Object object : parametros) {
			if (object != null)
				methodLog.append(object.getClass().getName()).append(" : ").append(object).append(", ");
		}
		int length = methodLog.length();
		methodLog.append(")");
		if (parametros.length != 0)
			methodLog.replace(length - 2, length - 1, "");
		return methodLog.toString();
	}

}
