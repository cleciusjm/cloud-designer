package com.clouddesigner.model.facade;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.clouddesigner.component.model.datatype.DataType;
import com.clouddesigner.component.model.datatype.DataTypeGroup;
import com.clouddesigner.component.model.datatype.DataTypeModel;
import com.clouddesigner.model.facade.interceptors.BenchmarkMethodInterceptor;

@LocalBean
@Stateless
@Interceptors(value = { BenchmarkMethodInterceptor.class })
public class DatabasesFacade {
	@PersistenceContext
	private EntityManager entityManager;

	@Lock(LockType.READ)
	public List<DataTypeModel> getAllDatabases() {
		return entityManager.createNamedQuery("DataTypeModel.getAll",
				DataTypeModel.class).getResultList();
	}

	public DataTypeModel insert(DataTypeModel database) {
		for (DataTypeGroup dtg : database.getGroups()) {
			dtg.setDatabase(database);
			for (DataType dtt : dtg.getTypes()) {
				dtt.setDtgroup(dtg);
			}
		}

		return entityManager.merge(database);
	}

	public DataTypeModel update(DataTypeModel database) {
		return entityManager.merge(database);
	}

	public void delete(DataTypeModel selectedDataType) {
		entityManager.remove(entityManager.merge(selectedDataType));
	}

	public DataTypeModel getDataBaseByName(String db) {
		return entityManager
				.createNamedQuery("DataTypeModel.getByName",
						DataTypeModel.class).setParameter("db", db)
				.setMaxResults(1).getSingleResult();
	}

}
