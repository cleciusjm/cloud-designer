package com.clouddesigner.model.facade;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

@Singleton
public class ParamsManagerFacade {

	private Map<InternalParameter, Object> paramsMap = Collections.synchronizedMap(new HashMap<InternalParameter, Object>());

	@PostConstruct
	protected void init() {
		paramsMap.put(InternalParameter.LOG_BENCHMARK, true);
	}

	public Map<InternalParameter, Object> getParamsMap() {
		return paramsMap;
	}

}
