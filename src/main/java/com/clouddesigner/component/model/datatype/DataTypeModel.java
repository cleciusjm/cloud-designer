package com.clouddesigner.component.model.datatype;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "database_config")
@XStreamAlias(value = "datatypes")
@NamedQueries({
		@NamedQuery(name = "DataTypeModel.getAll", query = "select d from DataTypeModel d"),
		@NamedQuery(name = "DataTypeModel.getByName", query = "select d from DataTypeModel d where d.db=:db") })
public class DataTypeModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@Column(name = "database_name")
	@XStreamAsAttribute
	private String db;

	@Lob
	@Column(name = "xml_to_sql_xsl")
	@XStreamOmitField
	private String xmlToSqlXsl;

	@OneToMany(mappedBy = "database", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@XStreamImplicit
	@OrderBy("id")
	private Set<DataTypeGroup> groups;

	public DataTypeModel() {
	}

	public DataTypeModel(String db) {
		this.groups = new HashSet<DataTypeGroup>();
		this.db = db;
	}

	public DataTypeModel(Set<DataTypeGroup> groups, String db) {
		this.groups = groups;
		this.db = db;
	}

	public Set<DataTypeGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<DataTypeGroup> groups) {
		this.groups = groups;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmlToSqlXsl() {
		return xmlToSqlXsl;
	}

	public void setXmlToSqlXsl(String xmlToSqlXsl) {
		System.out.printf("DataTypeModel.setXmlToSqlXsl(%s)\n", xmlToSqlXsl);
		this.xmlToSqlXsl = xmlToSqlXsl;
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		else
			return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (id == null || obj == null || !(obj instanceof DataTypeModel))
			return super.equals(obj);
		else
			return id.equals(((DataTypeModel) obj).getId());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass()
				.getSimpleName());
		builder.append("[");
		builder.append("id: ").append(id).append(", ");
		builder.append("database: ").append(db).append(", ");
		builder.append("xmlToSql : ").append(xmlToSqlXsl).append(", ");
		builder.append("groups : ").append(groups);
		builder.append("]");
		return builder.toString();
	}

}
