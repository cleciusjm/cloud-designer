package com.clouddesigner.component.model.datatype;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "datatype")
@XStreamAlias(value = "type")
public class DataType {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "dtgroup", insertable = true, updatable = true)
	@XStreamOmitField
	private DataTypeGroup dtgroup;

	@XStreamAsAttribute
	private String label;

	@XStreamAsAttribute
	private int length;

	@XStreamAsAttribute
	private String sql;

	@XStreamAsAttribute
	private String re;

	@XStreamAsAttribute
	private String quote;

	public DataType() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getRe() {
		return re;
	}

	public void setRe(String re) {
		this.re = re;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DataTypeGroup getDtgroup() {
		return dtgroup;
	}

	public void setDtgroup(DataTypeGroup dtgroup) {
		this.dtgroup = dtgroup;
	}
	
	


}
