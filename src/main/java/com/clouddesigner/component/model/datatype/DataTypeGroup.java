package com.clouddesigner.component.model.datatype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "datatype_group")
@XStreamAlias(value = "group")
public class DataTypeGroup {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "database")
	@XStreamOmitField
	private DataTypeModel database;

	@Column(name = "name")
	@XStreamAsAttribute
	private String label;

	@XStreamAsAttribute
	private String color;

	@OneToMany(mappedBy = "dtgroup", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@XStreamImplicit
	@OrderBy("label")
	private Set<DataType> types;

	public DataTypeGroup() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Set<DataType> getTypes() {
		return types;
	}

	public void setTypes(Set<DataType> types) {
		this.types = types;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DataTypeModel getDatabase() {
		return database;
	}

	public void setDatabase(DataTypeModel database) {
		this.database = database;
	}

}
