package com.clouddesigner.component.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "model_row")
@XStreamAlias(value = "row")
public class TableRow {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "table_id")
	@XStreamOmitField
	private ModelTable table;

	@XStreamAsAttribute
	private String name;

	@XStreamAsAttribute
	@XStreamAlias(value = "null")
	private String nullable;

	@XStreamAsAttribute
	private String autoincrement;
	
	@XStreamAsAttribute
	private String comment;

	private String datatype;

	@XStreamAlias(value = "default")
	@Column(name = "default_value")
	private String defaultValue;

	@OneToMany(mappedBy = "ownerRow", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@XStreamImplicit
	@OrderBy("id")
	private Set<Relation> relations = new HashSet<Relation>();

	public TableRow() {
		this.relations = new HashSet<Relation>();
	}

	public TableRow(String name, boolean nullable, boolean autoincrement, String datatype, String defaultValue, Set<Relation> relations) {
		this.name = name;
		this.setNullable(nullable);
		this.setAutoincrement(autoincrement);
		this.datatype = datatype;
		this.defaultValue = defaultValue;
		this.relations = relations;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isNullable() {
		return "1".equals(nullable);
	}

	public void setNullable(boolean nullable) {
		this.nullable = (nullable) ? ("1") : ("0");
	}

	public boolean isAutoincrement() {
		return "1".equals(autoincrement);
	}

	public void setAutoincrement(boolean autoincrement) {
		this.autoincrement = (autoincrement) ? ("1") : ("0");
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public Set<Relation> getRelations() {
		return relations;
	}

	public void setRelations(Set<Relation> relations) {
		this.relations = relations;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ModelTable getTable() {
		return table;
	}

	public void setTable(ModelTable table) {
		this.table = table;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
