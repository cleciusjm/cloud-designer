package com.clouddesigner.component.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias(value = "key")
@Entity
@Table(name = "constraint_def")
public class Key {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "owner_table")
	@XStreamOmitField
	private ModelTable table;

	@XStreamAsAttribute
	private String type;

	@XStreamAsAttribute
	private String name;
		
	@ElementCollection(targetClass = String.class, fetch=FetchType.EAGER)  
	@CollectionTable(name="key_part",joinColumns = @JoinColumn(name = "key_id"))  
    @Column(name = "part")  
	@XStreamImplicit(itemFieldName="part")
	private Set<String> part = new HashSet<String>();   

	public Key() {
	}

	public Key(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Set<String> getPart() {
		return part;
	}

	public void setPart(Set<String> part) {
		this.part = part;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ModelTable getTable() {
		return table;
	}

	public void setTable(ModelTable table) {
		this.table = table;
	}

}
