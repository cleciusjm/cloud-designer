package com.clouddesigner.component.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "model_table")
@XStreamAlias(value = "table")
public class ModelTable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "model")
	@XStreamOmitField
	private DbModel model;

	@XStreamAsAttribute
	private String name;
	
	@XStreamAsAttribute
	private String comment;

	@Column(name = "pos_x")
	@XStreamAsAttribute
	private int x;

	@Column(name = "pos_y")
	@XStreamAsAttribute
	private int y;

	@OneToMany(mappedBy = "table", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@XStreamImplicit
	@OrderBy("id")
	private Set<TableRow> rows = new HashSet<TableRow>();

	@OneToMany(mappedBy = "table", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@XStreamImplicit
	@OrderBy("name")
	private Set<Key> keys = new HashSet<Key>();

	public ModelTable() {
	}

	public ModelTable(String name, int x, int y) {
		this(name, x, y, new HashSet<TableRow>(), new HashSet<Key>());
	}

	public ModelTable(String name, int x, int y, Set<TableRow> rows, Set<Key> keys) {
		super();
		this.name = name;
		this.x = x;
		this.y = y;
		this.rows = rows;
		this.keys = keys;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TableRow> getRows() {
		return rows;
	}

	public void setRows(Set<TableRow> rows) {
		this.rows = rows;
	}

	public Set<Key> getKeys() {
		return keys;
	}

	public void setKeys(Set<Key> keys) {
		this.keys = keys;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DbModel getModel() {
		return model;
	}

	public void setModel(DbModel model) {
		this.model = model;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
