package com.clouddesigner.component.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
@XStreamAlias(value="relation")
@Entity
@Table(name = "relation")
public class Relation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="owner_row")
	@XStreamOmitField
	private TableRow ownerRow;
	
	@XStreamAsAttribute
	@Column(name="table_name")
	private String table;
	
	@XStreamAsAttribute
	@Column(name="row_name")
	private String row;

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TableRow getOwnerRow() {
		return ownerRow;
	}

	public void setOwnerRow(TableRow ownerRow) {
		this.ownerRow = ownerRow;
	}

}
