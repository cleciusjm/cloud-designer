package com.clouddesigner.component.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.clouddesigner.component.model.datatype.DataTypeModel;
import com.clouddesigner.entities.Project;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "model")
@XStreamAlias(value = "sql")
public class DbModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XStreamOmitField
	private Integer id;

	@OneToOne(mappedBy = "model")
	@XStreamOmitField
	private Project project;

	@ManyToOne
	@JoinColumn(name = "database")
	@XStreamAlias(value = "datatypes")
	private DataTypeModel database;

	@OneToMany(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@XStreamImplicit
	@OrderBy("name")
	private Set<ModelTable> tables = new HashSet<ModelTable>();

	public DbModel() {

	}

	public DbModel(DataTypeModel database) {
		this.database = database;
	}

	public DataTypeModel getDatabase() {
		return database;
	}

	public void setDatabase(DataTypeModel database) {
		this.database = database;
	}

	public Set<ModelTable> getTables() {
		return tables;
	}

	public void setTables(Set<ModelTable> tables) {
		this.tables = tables;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
