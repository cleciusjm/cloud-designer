package com.clouddesigner.component.translator;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.clouddesigner.util.log.Log;

public class ModelTranslator {

	private Log log = new Log(this.getClass());

	public ModelTranslator() {

	}

	public String transform(String xml, String xsl) throws TransformerException {
		StringWriter outTXT = new StringWriter();

		TransformerFactory factory = TransformerFactory.newInstance();

		StreamSource xslStream = new StreamSource(new StringReader(xsl));
		log.i("!!! XSL: {0}", xsl);
		log.i("!!! XML: {0}", xml);
		Transformer transformer = factory.newTransformer(xslStream);
		// transformer.setErrorListener(this);

		StreamSource in = new StreamSource(new StringReader(xml));
		StreamResult out = new StreamResult(outTXT);
		transformer.transform(in, out);

		return outTXT.toString();
	}

}
