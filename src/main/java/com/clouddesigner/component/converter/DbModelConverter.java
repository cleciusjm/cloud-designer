package com.clouddesigner.component.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.clouddesigner.component.model.DbModel;
import com.clouddesigner.xml.XmlParser;

@FacesConverter(forClass = DbModel.class, value = "dbModelConverter")
public class DbModelConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String xml) {
		try {
			if (xml != null && !xml.isEmpty()) {
				XmlParser<DbModel> parser = new XmlParser<>(DbModel.class);
				return parser.fromXML(xml);
			} else {
				return new DbModel();
			}
		} catch (Exception e) {
			throw new RuntimeException("Erro no converter.getAsObject [" + e.getMessage() + "]", e);
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object object) {
		try {
			XmlParser<DbModel> parser = new XmlParser<DbModel>(DbModel.class);
			return parser.toXML((DbModel) object);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Erro no converter.getAsString [" + e.getMessage() + "]", e);
		}

	}
}
