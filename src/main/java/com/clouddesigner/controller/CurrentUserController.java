package com.clouddesigner.controller;

import java.io.Serializable;
import java.security.Principal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.clouddesigner.entities.Role;
import com.clouddesigner.entities.User;
import com.clouddesigner.model.facade.UsersFacade;

@Named
@SessionScoped
public class CurrentUserController implements Serializable {

	private static final long serialVersionUID = 1L;

	private User usuario;

	@Inject
	private Principal userPrincipal;

	@Inject
	private UsersFacade usersFacade;

	@PostConstruct
	public void load() throws Exception {
		usuario = usersFacade.getByLogin(userPrincipal.getName());
	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

	public User getUsuario() {
		return usuario;
	}

	public boolean isLogado() {
		return this.getUsuario() != null;
	}

	public boolean isInRole(String role) {
		return FacesContext.getCurrentInstance().getExternalContext().isUserInRole(role);
	}

	public boolean isAdmin() {
		return isInRole("ADMIN");
	}

	public String getTheme() {
		if (usuario.getTheme() == null || usuario.getTheme().isEmpty())
			usuario.setTheme("black-tie");
		return usuario.getTheme();
	}

	public void setTheme(String theme) {
		usuario.setTheme(theme);
	}

	public void publishChange() {	
		usuario = usersFacade.update(usuario);
	}
	
	public String rolesUser(){
		StringBuffer roles = new StringBuffer();
		for(Role r: usuario.getRoles()){
			roles.append(r.getRoleName());
			roles.append(", ");
			
		}
		if(roles.length()>2)
			roles.replace(roles.length()-2, roles.length()-1, "");
		return roles.toString();
	}
	
	public String getLogin(){
		return usuario.getLogin();
	}
	
}
