package com.clouddesigner.controller;

import java.io.Serializable;

import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import com.clouddesigner.component.model.DbModel;
import com.clouddesigner.entities.Project;
import com.clouddesigner.entities.transients.Permission;
import com.clouddesigner.model.facade.DatabasesFacade;
import com.clouddesigner.model.facade.ProjectFacade;
import com.clouddesigner.model.facade.ProjectInstancesManagerFacade;
import com.clouddesigner.util.log.Log;

@Named
@ViewScoped
public class DesignerController implements Serializable {
	private static final long serialVersionUID = 1L;
	@Inject
	private Log log;
	@Inject
	private ProjectFacade projectsFacade;
	@Inject
	private CurrentUserController userController;
	@Inject
	private ProjectInstancesManagerFacade projectEditorFacade;
	@Inject
	private DatabasesFacade databasesFacade;

	private Integer id;
	private Integer idModel;

	private DbModel activeModel;

	private Permission permission;

	private Project project;

	public void loadModelFromId() {
		try {
			project = projectsFacade.getProjectByIdByUser(id,
					userController.getUsuario());
			if (project != null) {
				activeModel = project.getModel();
				idModel = activeModel.getId();
				this.permission = projectEditorFacade.openProject(
						projectsFacade.merge(project),
						userController.getUsuario());
			}
		} catch (EJBException e) {
			log.i("Projeto de id {0} não encontrado!", id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@PreDestroy
	public void closeProject() {
		System.out.println("DesignerController.closeProject()");
		projectEditorFacade.closeProject(project, userController.getUsuario());
	}

	public void saveModel() throws NotSupportedException, SystemException,
			SecurityException, IllegalStateException, RollbackException,
			HeuristicMixedException, HeuristicRollbackException {
		if (this.permission.canEdit()) {
			try {
				activeModel.setDatabase(databasesFacade
						.getDataBaseByName(activeModel.getDatabase().getDb()));
				project.setModel(activeModel);
				project = projectsFacade.insert(project);
				projectsFacade.removeModelById(idModel);
				idModel = project.getModel().getId();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, project
								.getName(), "Modelo salvo com sucesso!"));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO!",
								e.getMessage()));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Permissão Negada!", "Usuário "
									+ userController.getLogin()
									+ " sem permissão."));
		}
	}

	public DbModel getActiveModel() {
		if (activeModel == null)
			loadModelFromId();
		return activeModel;
	}

	public void setActiveModel(DbModel activeModel) {
		this.activeModel = activeModel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

}
