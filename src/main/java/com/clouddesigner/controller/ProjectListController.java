package com.clouddesigner.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.xml.transform.TransformerException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.clouddesigner.component.model.DbModel;
import com.clouddesigner.component.translator.ModelTranslator;
import com.clouddesigner.entities.Project;
import com.clouddesigner.exceptions.InvalidXmlEntryException;
import com.clouddesigner.model.facade.DatabasesFacade;
import com.clouddesigner.model.facade.ProjectFacade;
import com.clouddesigner.util.log.Log;
import com.clouddesigner.xml.XmlParser;

@Named
@ViewScoped
public class ProjectListController implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private Log log;

	@Inject
	private ProjectFacade projectFacade;

	@Inject
	private DatabasesFacade databasesFacade;

	@Inject
	private CurrentUserController userController;

	@Inject
	private ProjectCrudController crud;

	private Project selectedProject;

	private List<Project> projects;

	@PostConstruct
	public void loadProjects() {
		log.t("ProjectListController.loadProjects()");
		projects = projectFacade.getAllProjectList(userController.getUsuario());
		selectedProject = null;
	}

	public void onProjectSelect(SelectEvent event) throws IOException {
		Project project = (Project) event.getObject();
		log.t("ProjectListController.onProjectSelect({0})", project);
		try {
			crud.startEdit(project);
		} catch (Exception e) {
			log.e("Erro ao tentar iniciar a edição do projeto: {0}", e, project);
		}
	}

	public void onProjectUnselect() {
		log.t("ProjectListController.onProjectUnselect()");
		this.setSelectedProject(null);
	}

	public void onNewProject() {
		log.t("ProjectListController.onNewProject()");
		this.setSelectedProject(null);
		try {
			crud.startNew();
		} catch (Exception e) {
			log.e("Erro ao tentar iniciar um novo projeto", e);
		}
	}

	public void removeProject() {
		log.t("ProjectListController.removeProject()");
		crud.remove();
		this.loadProjects();
	}

	public void saveProject() {
		log.t("ProjectListController.saveProject()");
		crud.save();
		this.loadProjects();
	}

	public Project getSelectedProject() {
		log.t("ProjectListController.getSelectedProject() : {0}",
				selectedProject);
		return selectedProject;
	}

	public void setSelectedProject(Project selectedProject) {
		log.t("ProjectListController.setSelectedProject({0})", selectedProject);
		this.selectedProject = selectedProject;
	}

	public List<Project> getProjects() {
		log.t("ProjectListController.getProjects() : {0}", projects);
		return projects;
	}

	public void setProjects(List<Project> projects) {
		log.t("ProjectListController.setProjects({0})", projects);
		this.projects = projects;
	}

	public ProjectCrudController getCrud() {
		return crud;
	}

	public void setCrud(ProjectCrudController crud) {
		this.crud = crud;
	}

	public StreamedContent getDownloadXmlFile() throws InvalidXmlEntryException {
		Project project = projectFacade.merge(crud.getProject());
		XmlParser<DbModel> parser = new XmlParser<>(DbModel.class);
		InputStream stream = new ByteArrayInputStream(parser.toXML(
				(DbModel) project.getModel()).getBytes());
		return new DefaultStreamedContent(stream, "text/xml", project
				.getName() + ".xml");
	}

	public void upload(FileUploadEvent event) {
		try {
			if (event.getFile() == null)
				throw new Exception("file is null");

			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					event.getFile().getInputstream()));

			String lineRead = "";
			while ((lineRead = reader.readLine()) != null) {
				builder.append(lineRead);
			}
			XmlParser<DbModel> parser = new XmlParser<>(DbModel.class);
			try {
				DbModel model = parser.fromXML(builder.toString());
				model.setDatabase(databasesFacade.getDataBaseByName(model
						.getDatabase().getDb()));
				crud.getProject().setModel(model);
				FacesMessage msg = new FacesMessage("IMPORTAÇÃO", event
						.getFile().getFileName()
						+ " foi importado com sucesso!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} catch (NoResultException e) {
				log.d("NoResultException: {0}", e.getMessage());
				FacesMessage msg = new FacesMessage(
						"Erro ao obter banco de dados do arquivo ", event
								.getFile().getFileName()
								+ ". Banco não encontrado!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} catch (Exception e) {
				log.e("Erro ao importar arquivo1: {0}", e);
				FacesMessage msg = new FacesMessage(
						"Erro ao importar arquivo ", event.getFile()
								.getFileName() + ". Arquivo inválido!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception e) {
			log.e("Erro ao importar arquivo2: {0}", e);
			FacesMessage msg = new FacesMessage("ERRO",
					"Erro ao importar arquivo " + event.getFile().getFileName()
							+ ". " + e);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			e.printStackTrace();
		}
	}

	public StreamedContent getDownloadSqlFile() throws TransformerException,
			InvalidXmlEntryException {
		ModelTranslator modelTranslator = new ModelTranslator();
		XmlParser<DbModel> parser = new XmlParser<>(DbModel.class);
		Project project = projectFacade.merge(crud.getProject());
		String xml = parser.toXML(project.getModel());
		String xsl = project.getModel().getDatabase().getXmlToSqlXsl();
		InputStream stream = new ByteArrayInputStream(modelTranslator.transform(xml, xsl).getBytes());
		return new DefaultStreamedContent(stream, "text/xml", project
				.getName() + ".sql");
	}

}
