package com.clouddesigner.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.clouddesigner.entities.Message;

@Named
public class MessageBoxController implements Serializable {
	private static final long serialVersionUID = 1L;

	private TreeNode messageboxes;

	private List<Message> messages;

	private Message message;

	private TreeNode messagebox;

	@PostConstruct
	public void init() {
		messageboxes = new DefaultTreeNode("root", null);

		TreeNode inbox = new DefaultTreeNode("in", "Recebidas", messageboxes);
		inbox.setSelected(true);
		TreeNode sent = new DefaultTreeNode("out", "Enviadas", messageboxes);
		TreeNode trash = new DefaultTreeNode("trash", "Lixeira", messageboxes);

		messages = new ArrayList<Message>();
		messages.add(new Message("fernando", "fernandoDestino",
				"Visca el Barca", "BARCAAAAA!!!", new Date()));

	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public TreeNode getMessagebox() {
		return messagebox;
	}

	public void setMessagebox(TreeNode messagebox) {
		this.messagebox = messagebox;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public TreeNode getMessageboxes() {
		return messageboxes;
	}

	public void setMessageboxes(TreeNode messageboxes) {
		this.messageboxes = messageboxes;
	}

	public void send() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Mail Sent!"));
	}
}
