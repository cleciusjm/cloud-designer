package com.clouddesigner.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;

import org.primefaces.event.SelectEvent;

import com.clouddesigner.component.model.datatype.DataTypeModel;
import com.clouddesigner.controller.util.ControllerState;
import com.clouddesigner.entities.AllowedUser;
import com.clouddesigner.entities.Project;
import com.clouddesigner.entities.User;
import com.clouddesigner.entities.transients.Permission;
import com.clouddesigner.model.facade.DatabasesFacade;
import com.clouddesigner.model.facade.ProjectFacade;
import com.clouddesigner.model.facade.ProjectInstancesManagerFacade;
import com.clouddesigner.model.facade.UsersFacade;
import com.clouddesigner.util.log.Log;

public class ProjectCrudController implements Serializable {
	private static final long serialVersionUID = 1L;
	@Inject
	private Log log;
	@Inject
	private UsersFacade usersFacade;
	@Inject
	private DatabasesFacade databasesFacade;
	@Inject
	private ProjectFacade projectFacade;
	@Inject
	private ProjectInstancesManagerFacade projectEditorFacade;
	@Inject
	private CurrentUserController userController;
	private List<DataTypeModel> datatypes;
	private List<User> users;
	private Project project;
	private User userSelected;
	private Permission permission;
	private ControllerState state = ControllerState.NONE;

	@PostConstruct
	public void init() throws NotSupportedException, SystemException {
		log.t("ProjectCrudController.init() : " + state);
		datatypes = databasesFacade.getAllDatabases();
		users = usersFacade.getAllUsers();
	}

	public void startEdit(Project project) throws SystemException,
			NotSupportedException {
		if (isActive())
			this.finish();
		this.project = projectFacade.merge(project);
		users = usersFacade.getAllUsers();

		users.remove(userController.getUsuario());
		for (AllowedUser a : this.project.getAllowedUsers())
			users.remove(a.getUser());

		log.t("ProjectCrudController.startEdit({0})", project);
		this.permission = projectEditorFacade.openProject(this.project,
				userController.getUsuario());
		this.state = ControllerState.EDITING;
	}

	public void startNew() throws SystemException, NotSupportedException {
		if (isActive())
			this.finish();
		users = usersFacade.getAllUsers();
		users.remove(userController.getUsuario());
		log.t("ProjectCrudController.startNew()");
		this.project = new Project(userController.getUsuario());
		this.permission = new Permission(userController.getUsuario().getId(),
				Permission.OWNER);
		this.state = ControllerState.ADDING;
	}

	public String save() {
		log.t("ProjectCrudController.save({0})", project);
		try {
			project = projectFacade.insert(project);
			state = ControllerState.NONE;
			return "/pages/user/designer.xhtml?faces-redirect=true&id="
					+ project.getId();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Erro ao salvar o projeto",
							"Foi interceptado o seguinte erro ao tentar salvar o projeto:\n"
									+ e.getClass().getName() + "["
									+ e.getMessage()
									+ "]\nPara mais detalhes veja o log"));
			log.e("Erro ao salvar o projeto {0}", e, project);
			return null;
		}

	}

	public void remove() {
		log.t("ProjectCrudController.remove({0})", project);
		if (isEdit()) {
			try {
				projectFacade.remove(project);
				state = ControllerState.NONE;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Erro ao deletar o projeto",
								"Foi interceptado o seguinte erro ao tentar deletar o projeto:\n"
										+ e.getClass().getName() + "["
										+ e.getMessage()
										+ "]\nPara mais detalhes veja o log"));
				log.e("Erro ao salvar o projeto {0}", e, project);
			}
		}
	}

	public void setDatabase(DataTypeModel database) {
		log.t("ProjectCrudController.setDatabase({0})", database);
		project.getModel().setDatabase(database);
	}

	public DataTypeModel getDatabase() {
		log.t("ProjectCrudController.getDatabase() : {0}", project.getModel()
				.getDatabase());
		return project.getModel().getDatabase();
	}

	@PreDestroy
	public void finish() throws IllegalStateException, SecurityException,
			SystemException {
		log.t("ProjectCrudController.finish()");
	}

	public void removeUser() {
		log.t("ProjectCrudController.removeUser({0})", userSelected);
		for (AllowedUser usersPermission : project.getAllowedUsers()) {
			log.t("analisando {0}", usersPermission);
			if (usersPermission.getUser().getId().equals(userSelected.getId())) {
				log.t("removendo {0}", usersPermission);
				project.getAllowedUsers().remove(usersPermission);
				projectFacade.removeAllowedUser(usersPermission);
				break;
			}
		}

		users.add(userSelected);
		userSelected = null;
	}

	public void onUserSelect(SelectEvent event) {
		User user = ((User) event.getObject());
		log.t("ProjectCrudController.onUserSelect({0})", user);
		project.getAllowedUsers().add(new AllowedUser(false, project, user));
		users.remove(userSelected);
		userSelected = null;
	}

	public Project getProject() {
		log.t("ProjectCrudController.getProject() : {0}", project);
		return project;
	}

	public void setProject(Project project) {
		log.t("ProjectCrudController.setProject({0})", project);
		this.project = project;
	}

	public List<AllowedUser> getAllowedUser() {
		log.t("ProjectCrudController.getAllowedUser() : {0}",
				project.getAllowedUsers());
		return project.getAllowedUsers();
	}

	public void setAllowedUser(List<AllowedUser> allowedUsers) {
		log.t("ProjectCrudController.setAllowedUser({0})", allowedUsers);
		project.setAllowedUsers(allowedUsers);
	}

	public List<User> getUsers() {
		log.t("ProjectCrudController.getUsers() : {0}", users);
		return users;
	}

	public void setUsers(List<User> users) {
		log.t("ProjectCrudController.setUsers({0})", users);
		this.users = users;
	}

	public List<DataTypeModel> getDatatypes() {
		log.t("ProjectCrudController.getDatatypes() : {0}", datatypes);
		return datatypes;
	}

	public void setDatatypes(List<DataTypeModel> datatypes) {
		log.t("ProjectCrudController.setDatatypes({0})", datatypes);
		this.datatypes = datatypes;
	}

	public User getUserSelected() {
		log.t("ProjectCrudController.getUserSelected() : {0}", userSelected);
		return userSelected;
	}

	public void setUserSelected(User userSelected) {
		log.t("ProjectCrudController.setUserSelected({0})", userSelected);
		this.userSelected = userSelected;
	}

	public Permission getPermission() {
		log.t("ProjectCrudController.getPermission() : {0}", permission);
		return permission;
	}

	public void setPermission(Permission permission) {
		log.t("ProjectCrudController.setPermission({0})", permission);
		this.permission = permission;
	}

	public ControllerState getState() {
		return state;
	}

	public void setState(ControllerState state) {
		this.state = state;
	}

	public boolean isActive() {
		boolean active = !state.equals(ControllerState.NONE);
		log.t("ProjectCrudController.isActive() : {0}", active);
		return active;
	}

	public boolean isNew() {
		boolean adding = state.equals(ControllerState.ADDING);
		log.t("ProjectCrudController.isNew() : {0}", adding);
		return adding;
	}

	public boolean isEdit() {
		boolean editing = state.equals(ControllerState.EDITING);
		log.t("ProjectCrudController.isEdit() : {0}", editing);
		return editing;
	}

}
