package com.clouddesigner.controller.converter;

import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.clouddesigner.component.model.datatype.DataTypeModel;

@FacesConverter(value = "dataTypeModelConverter")
public class DataTypeModelConverter implements Converter {

	private static  Map<String, DataTypeModel> index = new HashMap<>();

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String string) {
		return getIndex().get(string);
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object obj) {
		if (obj instanceof DataTypeModel) {
			DataTypeModel model = ((DataTypeModel) obj);
			getIndex().put(model.getDb(), model);
			return model.getDb();
		} else {
			return null;
		}
	}
	
	public void put(String key, DataTypeModel databaseface){
		
	}

	public Map<String, DataTypeModel> getIndex() {
		// if (this.index == null) {
		// HttpSession session = (HttpSession)
		// FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		// if (session.getAttribute("dataTypeModelConverterIndex") == null) {
		// session.setAttribute("dataTypeModelConverterIndex", new HashMap<>());
		// }
		// this.index = (Map<String, DataTypeModel>)
		// session.getAttribute("dataTypeModelConverterIndex");
		// }
		return index;
	}
}
