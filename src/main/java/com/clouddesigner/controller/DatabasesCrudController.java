package com.clouddesigner.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.transform.TransformerException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import com.clouddesigner.component.model.datatype.DataType;
import com.clouddesigner.component.model.datatype.DataTypeGroup;
import com.clouddesigner.component.model.datatype.DataTypeModel;
import com.clouddesigner.controller.util.ControllerState;
import com.clouddesigner.exceptions.InvalidXmlEntryException;
import com.clouddesigner.model.facade.DatabasesFacade;
import com.clouddesigner.util.log.Log;
import com.clouddesigner.xml.XmlParser;

@Named
@ViewScoped
public class DatabasesCrudController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Log log;

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private DatabasesFacade databasesFacade;

	@SuppressWarnings("cdi-ambiguous-dependency")
	@Inject
	private XmlParser<DataTypeModel> parser;

	private TreeNode root;

	private DataTypeModel selectedDataType;

	private String xmlToSqlXsl;

	private List<DataTypeModel> loadedDataTypes;

	private ControllerState state = ControllerState.NONE;

	@PostConstruct
	public void loadData() {
		try {
			loadedDataTypes = (databasesFacade.getAllDatabases());
		} catch (Exception e) {
			System.out
					.println("AdminOptionsController.init(Failed to load all Databases)");
		}
	}

	public void startNew() {
		log.t("DatabasesCrudController.startNew()");
		setState(ControllerState.ADDING);
		this.selectedDataType = new DataTypeModel();
	}

	public void startEdit(DataTypeModel database) {
		log.t("DatabasesCrudController.startEdit({0})", database);
		setState(ControllerState.EDITING);
		this.selectedDataType = database;
		this.xmlToSqlXsl = database.getXmlToSqlXsl();
		mountDbView();
	}

	public void save() {
		log.t("DatabasesCrudController.save()");
		if (isActive()) {
			selectedDataType.setXmlToSqlXsl(getXmlToSqlXsl());
			switch (state) {
			case ADDING:
				selectedDataType = databasesFacade.insert(selectedDataType);
				log.d("insert [{0}]", selectedDataType);
				break;
			case EDITING:
				selectedDataType = databasesFacade.update(selectedDataType);
				log.d("update [{0}]", selectedDataType);
				break;
			default:
				log.w("Estado [{0}] é inválido para a operação salvar", state);
				break;
			}
		} else {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_WARN,
									"Não foi possível salvar, o estado da aplicação é inválido",
									"O estado ["
											+ state
											+ "] é inválido para operação salvar"));
		}
		setState(ControllerState.NONE);
	}

	public void remove() {
		log.t("DatabasesCrudController.remove()");
		databasesFacade.delete(selectedDataType);
		setState(ControllerState.NONE);
	}

	private String loadFromStream(InputStream file) throws Exception {
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					file));

			String lineRead = "";
			while ((lineRead = reader.readLine()) != null) {
				builder.append(lineRead);
			}

			return builder.toString();
		} catch (Exception e) {
			throw new Exception("Failed to set details of file ["
					+ e.getMessage() + "]", e);
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		log.t("DatabasesCrudController.handleFileUpload({0})", event);

		try {
			if (isActive()) {
				if (event.getFile().getFileName().endsWith(".xml")) {
					selectedDataType = new DataTypeModel();
					parser.toXML(selectedDataType);
					setSelectedDataType((DataTypeModel) parser
							.fromXML(loadFromStream(event.getFile()
									.getInputstream())));

					mountDbView();
				} else if (event.getFile().getFileName().endsWith(".xsl")) {
					setXmlToSqlXsl(loadFromStream(event.getFile()
							.getInputstream()));
				} else {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_WARN,
									"Extensão de arquivo não conhecida",
									"Ignorando o arquivo submetido"));
				}
			} else {
				log.w("Upload de arquivo [{0}] efetuado com crud em estado {1}",
						event.getFile().getFileName(), state);
			}
		} catch (Exception e) {
			log.e("Erro ao gerenciar o arquivo ({0})", e, e.getMessage());
		}
	}

	private void mountDbView() {
		try {
			if (selectedDataType == null)
				throw new RuntimeException("DataType is null");
			root = new DefaultTreeNode("Root", null);
			TreeNode dbTypeNode = new DefaultTreeNode("BD - "
					+ selectedDataType.getDb(), root);

			TreeNode groupNode = null;
			for (DataTypeGroup dtGroup : selectedDataType.getGroups()) {
				groupNode = new DefaultTreeNode(
						"Grupo - " + dtGroup.getLabel(), dbTypeNode);

				for (DataType groupType : dtGroup.getTypes()) {
					new DefaultTreeNode("Tipo - " + groupType.getLabel(),
							groupNode);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to set file tree view ["
					+ e.getMessage() + "]");
		}
	}
	
	public StreamedContent downloadXsl() throws TransformerException, InvalidXmlEntryException {
		String xsl = selectedDataType.getXmlToSqlXsl();
		InputStream stream = new ByteArrayInputStream(xsl.getBytes());
		return new DefaultStreamedContent(stream, "text/xml", selectedDataType.getDb() + ".xsl");
	}
	
	
	
	public StreamedContent downloadXmlDatatype() throws InvalidXmlEntryException {
		XmlParser<DataTypeModel> parser = new XmlParser<>(DataTypeModel.class);
		InputStream stream = new ByteArrayInputStream(parser.toXML((DataTypeModel) selectedDataType).getBytes());
		return new DefaultStreamedContent(stream, "text/xml", selectedDataType.getDb() + " - datatype" + ".xml");
	}

	public void onRowSelect(SelectEvent event) {
		log.t("DatabaseCrudController.onRowSelect({0})", event.getObject());
		startEdit((DataTypeModel) event.getObject());
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public DataTypeModel getSelectedDataType() {
		return selectedDataType;
	}

	public void setSelectedDataType(DataTypeModel selected) {
		if (selected != null)
			this.selectedDataType = selected;
	}

	public List<DataTypeModel> getLoadedDataTypes() {
		return loadedDataTypes;
	}

	public void setLoadedDataTypes(List<DataTypeModel> loadedDataTypes) {
		this.loadedDataTypes = loadedDataTypes;
	}

	public boolean isActive() {
		boolean active = !getState().equals(ControllerState.NONE);
		log.t("DatabasesCrudController.isActive() : {0}", active);
		return active;
	}

	public boolean isNew() {
		boolean adding = getState().equals(ControllerState.ADDING);
		log.t("DatabasesCrudController.isNew() : {0}", adding);
		return adding;
	}

	public boolean isEdit() {
		boolean editing = getState().equals(ControllerState.EDITING);
		log.t("DatabasesCrudController.isEdit() : {0}", editing);
		return editing;
	}

	public ControllerState getState() {
		return state;
	}

	public void setState(ControllerState state) {
		log.t("DatabasesCrudController.setState({0})", state);
		this.root = null;
		this.selectedDataType = null;
		this.xmlToSqlXsl = null;
		this.state = state;
		this.loadData();
	}

	public String getXmlToSqlXsl() {
		return xmlToSqlXsl;
	}

	public void setXmlToSqlXsl(String xmlToSqlXsl) {
		this.xmlToSqlXsl = xmlToSqlXsl;
	}
}
