package com.clouddesigner.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import com.clouddesigner.controller.util.ControllerState;
import com.clouddesigner.entities.Role;
import com.clouddesigner.entities.User;
import com.clouddesigner.model.facade.RolesFacade;
import com.clouddesigner.model.facade.UsersFacade;
import com.clouddesigner.util.log.Log;

@Named
@ViewScoped
public class UsersCrudController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsersFacade userFacade;

	@Inject
	private RolesFacade roleFacade;

	@Inject
	private Log log;

	private List<User> availableUsers = new ArrayList<User>();

	private List<Role> availableRolesInstance = new ArrayList<Role>();

	private Map<String, String> availableRoles = new HashMap<String, String>();

	private List<String> selectedRoles = new ArrayList<String>();

	private User selectedUser;

	private OperationsController opController = new OperationsController();

	@PostConstruct
	public void init() {
		try {
			setSelectedUser(new User());

			availableUsers.addAll(userFacade.getAllUsers());

			for (Role r : roleFacade.getAllRoles()) {
				availableRoles.put(r.getRoleName(), String.valueOf(r.getId()));
				availableRolesInstance.add(r);
			}
		} catch (Exception e) {
			log.e("Falha ao iniciar controlador de usuários [{0}]", e,
					e.getMessage());
		}
	}

	public void onRowSelect(SelectEvent event) {
		opController.startEditing();

		setSelectedUser((User) event.getObject());
	}

	public void actionSaveUser() {
		try {
			User addedUser;

			selectedUser.setRoles(new HashSet<Role>());

			for (String roleId : selectedRoles) {

				for (Role r : availableRolesInstance) {

					if (r.getId() == Integer.parseInt(roleId)) {
						selectedUser.getRoles().add(r);
					}
				}
			}

			if (opController.getState().equals(ControllerState.ADDING)) {
				addedUser = userFacade.insert(selectedUser);

				availableUsers.add(addedUser);
			} else {
				addedUser = userFacade.update(selectedUser);

				availableUsers.remove(selectedUser);
				availableUsers.add(addedUser);
			}

			opController.finishAdding();

		} catch (Exception e) {
			log.e("Falha na operação de inserção de usuário[{0}]", e,
					e.getMessage());
		}

	}

	public void actionNewUser() {
		opController.startAdding();

		setSelectedUser(new User());
	}

	public void actionDeleteUser() {
		opController.startDeleting();

		try {
			userFacade.remove(selectedUser);
			availableUsers.remove(selectedUser);
		} catch (Exception e) {
			log.e("Falha na operação de deleção de usuário [{0}]", e,
					e.getMessage());
		}
	}

	public boolean isRoleAttached(Integer roleId) {

		if ((roleId != null) && (getSelectedUser() != null))
			for (Role r : getSelectedUser().getRoles()) {
				if (r.getId() == roleId)
					return true;
			}

		return false;
	}

	public String rolesUser(User user) {
		StringBuffer roles = new StringBuffer();
		for (Role r : user.getRoles()) {
			roles.append(r.getRoleName());
			roles.append(", ");

		}
		if (roles.length() > 2)
			roles.replace(roles.length() - 2, roles.length() - 1, "");
		return roles.toString();
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User user) {
		if (user != null)
			this.selectedUser = user;
	}

	public Map<String, String> getAvailableRoles() {
		return availableRoles;
	}

	public void setAvailableRoles(Map<String, String> availableRoles) {
		this.availableRoles = availableRoles;
	}

	public List<String> getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(List<String> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public List<Role> getAvailableRolesInstance() {
		return availableRolesInstance;
	}

	public void setAvailableRolesInstance(List<Role> availableRolesInstance) {
		this.availableRolesInstance = availableRolesInstance;
	}

	public List<User> getAvailableUsers() {
		return availableUsers;
	}

	public OperationsController getOpController() {
		return opController;
	}

	public void setOpController(OperationsController opController) {
		this.opController = opController;
	}

	public void setAvailableUsers(List<User> availableUsers) {
		this.availableUsers = availableUsers;
	}

	public class OperationsController {
		private boolean newDisabled = false;

		private boolean saveDisabled = true;

		private boolean deleteDisabled = true;

		private ControllerState state = ControllerState.NONE;

		public void startEditing() {
			setSaveDisabled(false);
			setDeleteDisabled(false);
			setNewDisabled(false);

			state = ControllerState.EDITING;
		}

		public void startDeleting() {
			setSaveDisabled(true);
			setDeleteDisabled(true);
			setNewDisabled(false);

			state = ControllerState.REMOVING;
		}

		public void startAdding() {
			setSaveDisabled(false);
			setDeleteDisabled(true);
			setNewDisabled(false);

			state = ControllerState.ADDING;
		}

		public void finishAdding() {
			setSaveDisabled(true);
			setDeleteDisabled(true);
			setNewDisabled(false);

			state = ControllerState.NONE;
		}

		public boolean isNewDisabled() {
			return newDisabled;
		}

		public void setNewDisabled(boolean newDisabled) {
			this.newDisabled = newDisabled;
		}

		public boolean isSaveDisabled() {
			return saveDisabled;
		}

		public void setSaveDisabled(boolean saveDisabled) {
			this.saveDisabled = saveDisabled;
		}

		public boolean isDeleteDisabled() {
			return deleteDisabled;
		}

		public void setDeleteDisabled(boolean deleteDisabled) {
			this.deleteDisabled = deleteDisabled;
		}

		public ControllerState getState() {
			return state;
		}

		public void setState(ControllerState state) {
			this.state = state;
		}
	}
}
