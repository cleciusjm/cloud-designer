package com.clouddesigner.controller.util;

public enum ControllerState {

	ADDING,
	REMOVING,
	EDITING,
	NONE
}
