package com.clouddesigner.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named
public class OptionsController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer snapDistance = 0;
	private String foreignNamesPattern = "%R_%T";
	private Integer snapDistanceTip = 0;
	private String foreignNamesTip = "%R_%T";
	private boolean hideConnectors = false;
	private boolean drawSmoothConnectors = true;
	private boolean showFieldSize = false;
	private boolean showDatatype = true;
	
	public void showFields(){
		System.out.println("OptionsController.showFields("+this.toString()+")");
	}
	
	public Integer getSnapDistance() {
		return snapDistance;
	}
	public void setSnapDistance(Integer snapDistance) {
		this.snapDistance = snapDistance;
	}

	public String getForeignNamesPattern() {
		return foreignNamesPattern;
	}
	public void setForeignNamesPattern(String foreignNamesPattern) {
		this.foreignNamesPattern = foreignNamesPattern;
	}

	public Integer getSnapDistanceTip() {
		return snapDistanceTip;
	}

	public void setSnapDistanceTip(Integer snapDistanceTip) {
		this.snapDistanceTip = snapDistanceTip;
	}

	public String getForeignNamesTip() {
		return foreignNamesTip;
	}

	public void setForeignNamesTip(String foreignNamesTip) {
		this.foreignNamesTip = foreignNamesTip;
	}

	public boolean isHideConnectors() {
		return hideConnectors;
	}

	public void setHideConnectors(boolean hideConnectors) {
		this.hideConnectors = hideConnectors;
	}

	public boolean isDrawSmoothConnectors() {
		return drawSmoothConnectors;
	}

	public void setDrawSmoothConnectors(boolean drawSmoothConnectors) {
		this.drawSmoothConnectors = drawSmoothConnectors;
	}

	public boolean isShowFieldSize() {
		return showFieldSize;
	}

	public void setShowFieldSize(boolean showFieldSize) {
		this.showFieldSize = showFieldSize;
	}

	public boolean isShowDatatype() {
		return showDatatype;
	}

	public void setShowDatatype(boolean showDatatype) {
		this.showDatatype = showDatatype;
	}

	@Override
	public String toString() {
		return "[snapDistance=" + snapDistance
				+ ", foreignNamesPattern=" + foreignNamesPattern
				+ ", hideConnectors=" + hideConnectors
				+ ", drawSmoothConnectors=" + drawSmoothConnectors
				+ ", showFieldSize=" + showFieldSize + ", showDatatype="
				+ showDatatype + "]";
	}
	
	

}
