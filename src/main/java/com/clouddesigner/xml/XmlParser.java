package com.clouddesigner.xml;

import java.io.Serializable;

import com.clouddesigner.exceptions.InvalidXmlEntryException;
import com.thoughtworks.xstream.XStream;

public class XmlParser<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	private XStream parser = new XStream();

	public XmlParser(Class<T> classe) throws InvalidXmlEntryException {
		parser.autodetectAnnotations(true);
		try {
			parser.toXML(classe.newInstance());
		} catch (Exception e) {
			throw new InvalidXmlEntryException(classe.getName()
					+ " não é um objeto serializavel em XML", e);
		}
	}

	public String toXML(T xml) {
		return parser.toXML(xml);
	}

	@SuppressWarnings("unchecked")
	public T fromXML(String xml) {
		return (T) parser.fromXML(xml);
	}
}
