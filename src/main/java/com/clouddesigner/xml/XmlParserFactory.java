package com.clouddesigner.xml;

import java.lang.reflect.ParameterizedType;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import com.clouddesigner.exceptions.InvalidXmlEntryException;

public class XmlParserFactory {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Produces
	public XmlParser getConverter(InjectionPoint point) throws InvalidXmlEntryException {
		ParameterizedType type = (ParameterizedType) point.getType();
		Class classe = (Class) type.getActualTypeArguments()[0];
		return new XmlParser<>(classe);
	}
}
