package com.clouddesigner.exceptions;

public class InvalidXmlEntryException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidXmlEntryException() {
		super();
	}

	public InvalidXmlEntryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidXmlEntryException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidXmlEntryException(String message) {
		super(message);
	}

	public InvalidXmlEntryException(Throwable cause) {
		super(cause);
	}

}
