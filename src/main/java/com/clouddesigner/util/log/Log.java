package com.clouddesigner.util.log;

import java.io.Serializable;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

public class Log implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Logger logger;

	private final Class<? extends Object> owner;

	public Log(Class<? extends Object> owner) {
		this.owner = owner;
		logger = Logger.getLogger(owner.getName());
	}

	public void i(String str, Object... params) {
		getLogger().logv(Level.INFO, str, params);
	}

	public void t(String str, Object... params) {
		getLogger().logv(Level.TRACE, str, params);
	}

	public void d(String str, Object... params) {
		getLogger().logv(Level.DEBUG, str, params);
	}

	public void w(String str, Object... params) {
		getLogger().logv(Level.WARN, str, params);
	}

	public void e(String str, Object... params) {
		getLogger().logv(Level.ERROR, str, params);
	}

	public void e(String str, Throwable e, Object... params) {
		getLogger().logv(Level.ERROR, e, str, params);
	}

	public Class<? extends Object> getOwner() {
		return owner;
	}

	public Logger getLogger() {
		return logger;
	}

}
