package com.clouddesigner.util.log;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

public class LogFactory {
	
	
	@Produces
	public Log producesLog(InjectionPoint injectionPoint) {
		Log log = new Log(injectionPoint.getMember().getDeclaringClass());
		return log;
	}
}
