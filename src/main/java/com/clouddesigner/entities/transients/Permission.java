package com.clouddesigner.entities.transients;

public class Permission {
	public static final int OWNER = 40;
	public static final int EDIT = 30;
	public static final int READ = 20;
	public static final int NONE = 10;

	private int userId;
	private int permission;

	public Permission(int userId, int permission) {
		this.userId = userId;
		this.permission = permission;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public boolean isOwner() {
		return (permission == OWNER);
	}

	public boolean canEdit() {
		return (permission >= EDIT);
	}

	public boolean canRead() {
		return (permission >= READ);
	}

	public boolean isNone() {
		return (permission == NONE);
	}

	@Override
	public int hashCode() {
		return userId;
	}

}
