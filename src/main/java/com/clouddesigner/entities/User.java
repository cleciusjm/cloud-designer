package com.clouddesigner.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name = "users")
@NamedQueries({
		@NamedQuery(name = "User.getByLogin", query = "select u from User u where u.login=:login"),
		@NamedQuery(name = "User.getAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.getById", query = "select u from User u where u.id=:id") })
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String login;

	private String name;

	private String password;

	private String theme;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<AllowedUser> sharedProjects;

	@OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Project> ownedProjects;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = { @JoinColumn(name = "id_user") }, inverseJoinColumns = { @JoinColumn(name = "id_roles") })
	private Set<Role> roles;

	public User() {
	}

	public User(String login, String name) {
		this.login = login;
		this.name = name;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<AllowedUser> getSharedProjects() {
		return sharedProjects;
	}

	public void setSharedProjects(Set<AllowedUser> sharedProjects) {
		this.sharedProjects = sharedProjects;
	}

	public Set<Project> getOwnedProjects() {
		return ownedProjects;
	}

	public void setOwnedProjects(Set<Project> ownedProjects) {
		this.ownedProjects = ownedProjects;
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		else
			return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (id == null || obj == null || !(obj instanceof User))
			return super.equals(obj);
		else
			return id.equals(((User) obj).getId());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass()
				.getSimpleName()).append(" ");
		builder.append("[");
		builder.append("id: ").append(id).append(", ");
		builder.append("login: ").append(login).append(", ");
		builder.append("name: ").append(name).append(", ");
		builder.append("password_size: ").append(password.length());
		builder.append("]");

		return builder.toString();
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}
}