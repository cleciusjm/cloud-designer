package com.clouddesigner.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the allowed_users database table.
 * 
 */
@Entity
@Table(name = "allowed_users")
public class AllowedUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "read_only")
	private boolean readOnly;

	@ManyToOne
	@JoinColumn(name = "project")
	private Project project;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public AllowedUser() {
	}

	public AllowedUser(Boolean readOnly, Project project, User user) {
		this.readOnly = readOnly;
		this.project = project;
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append("[");
		builder.append("id: ").append(id).append(", ");
		builder.append("user: ").append(user).append(", ");
		builder.append("project: ").append(project).append(", ");
		builder.append("readonly :").append(readOnly);
		builder.append("]");
		return builder.toString();
	}
}