package com.clouddesigner.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.clouddesigner.component.model.DbModel;

/**
 * The persistent class for the projects database table.
 * 
 */
@Entity
@Table(name = "projects")
@NamedQueries({
		@NamedQuery(name = "Project.getByOwner", query = "select p from Project p where p.owner=:user"),
		@NamedQuery(name = "Project.getById", query = "select p from Project p where p.id=:id"),
		@NamedQuery(name = "Project.getByIdByOwner", query = "select p from Project p where p.id=:id and p.owner=:user"),
		@NamedQuery(name = "Project.getByOwnerByAllowedUser", query = "SELECT distinct pr from Project pr LEFT JOIN pr.allowedUsers al where al.user = (SELECT u from User u where u = al.user and u=:user) or pr.owner = :user"),
		@NamedQuery(name = "Project.getByIdByOwnerByAllowedUser", query = "SELECT pr from Project pr LEFT JOIN pr.allowedUsers al where pr.id=:id and (al.user = (SELECT u from User u where u = al.user and u=:user) or pr.owner = :user)") })
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "creation_date")
	private Date creationDate = new Date();

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "model")
	private DbModel model = new DbModel();

	private String name;

	// bi-directional many-to-one association to AllowedUser
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<AllowedUser> allowedUsers = new ArrayList<>();

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "owner")
	private User owner;

	public Project() {
	}

	public Project(User usuario) {
		this.owner = usuario;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public DbModel getModel() {
		return this.model;
	}

	public void setModel(DbModel model) {
		this.model = model;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AllowedUser> getAllowedUsers() {
		return allowedUsers;
	}

	public void setAllowedUsers(List<AllowedUser> allowedUsers) {
		this.allowedUsers = allowedUsers;
	}

	public User getOwner() {
		return this.owner;
	}

	public void setOwner(User user) {
		this.owner = user;
	}

}